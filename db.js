const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  database: "monthly_budget",
  password: "postgres",
  port: 5433,
});

module.exports = pool;
